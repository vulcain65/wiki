---
title: "maj image"
date: 2017-10-17T15:26:15Z
draft: false
weight: 20s
---

## mise a jour image
``` sh
# mise a jour
docker-compose pull
# Relance containers
docker-compose up -d --remove-orphans
# Suppression images obsolètes
docker image prune
```